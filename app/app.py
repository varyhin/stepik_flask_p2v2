# как вариант перевести list teachers в dict json.loads(teachers) и дальше все переделать под словарь

from data import teachers

import uuid
import random

from flask import Flask
from flask import render_template
from flask import request

from flask_sqlalchemy import SQLAlchemy

from flask_wtf import FlaskForm
from wtforms import RadioField, StringField
from wtforms.validators import DataRequired

app = Flask(__name__)
# app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///sqlite3.db" # База в файле 
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///:memory:" # База в памяти
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config['SECRET_KEY'] = str(uuid.uuid4())


db = SQLAlchemy(app)

# Cоздание таблиц
class Teachers(db.Model):
    tablename = "teachers"
    uid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    about = db.Column(db.String)
    rating = db.Column(db.String)
    picture = db.Column(db.String)
    price = db.Column(db.String)
    goals = db.Column(db.String)
    free = db.Column(db.String)

class Goals(db.Model):
    tablename = "goals"   
    uid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    ru_name = db.Column(db.String)
    emoji = db.Column(db.String(10))

class Bookings(db.Model):
    tablename = "bookings"
    uid = db.Column(db.Integer, primary_key=True)
    client_week_day = db.Column(db.String)
    client_time = db.Column(db.String)
    client_teacher = db.Column(db.String)
    client_name = db.Column(db.String)
    client_phone = db.Column(db.String)

class Requests(db.Model):
    tablename = "requests"
    uid = db.Column(db.Integer, primary_key=True)
    request_goal = db.Column(db.String)
    request_time = db.Column(db.String)
    request_client_name = db.Column(db.String)
    request_client_phone = db.Column(db.String)

db.create_all()
db.session.commit()

# Наполнение созданных таблиц
for profile in teachers:
    teacher = Teachers(
        name=profile["name"], 
        about=profile["about"], 
        rating=profile["rating"], 
        picture=profile["picture"], 
        price=profile["price"], 
        goals=str(profile["goals"]), 
        free=str(profile["free"]))
    db.session.add(teacher)
db.session.commit()

db.session.add(Goals(name="travel", ru_name="Для путешествий", emoji="⛱️"))
db.session.add(Goals(name="study", ru_name="Для учебы", emoji="📘"))
db.session.add(Goals(name="work", ru_name="Для работы", emoji="👔"))
db.session.add(Goals(name="relocate", ru_name="Для переезда", emoji="📦"))
db.session.commit()

week_days_ru_dict = {
    'mon': 'Понедельник', 
    'tue': 'Вторник', 
    'wed': 'Среда', 
    'thu': 'Четверг', 
    'fri': 'Пятница', 
    'sat': 'Суббота', 
    'sun': 'Воскресенье'}

@app.route("/")
def render_main():
    return render_template("index.html",
    teachers = random.sample(db.session.query(Teachers).all(), 6),
    goals = db.session.query(Goals).all()
    )

@app.route("/goals/<goal>/")
def render_goal(goal):
    return render_template("goal.html",
    goal = goal,
    teachers = db.session.query(Teachers).all(),
    goals = db.session.query(Goals).all()
    )

@app.route("/profiles/<profile_id>/")
def render_profiles(profile_id):
    profile = db.session.query(Teachers).get(int(profile_id))
    goals = db.session.query(Goals).all()

    return render_template("profile.html",
    profile = profile,
    goals = goals,
    profile_goals = eval(profile.goals),
    timetable = eval(profile.free),
    week_days_ru_dict = week_days_ru_dict,
    )

@app.route("/booking/<profile_id>/<booking_day>/<booking_time>/")
def render_booking(profile_id, booking_day, booking_time):

    class BookingForm(FlaskForm):
        client_name = StringField('client_name', validators=[DataRequired()])
        client_phone = StringField('client_phone', validators=[DataRequired()])
        client_week_day = StringField('client_week_day', validators=[DataRequired()])
        client_time = StringField('client_time', validators=[DataRequired()])
        client_teacher = StringField('client_teacher', validators=[DataRequired()])

    form = BookingForm()
    profile = db.session.query(Teachers).get(int(profile_id))

    return render_template("booking.html",
    booking_day = booking_day,
    booking_time = booking_time,
    form = form,
    profile = profile,
    week_days_ru_dict = week_days_ru_dict
    )
    
@app.route("/booking_done/", methods=["POST"])
def render_booking_done():

    client_week_day = request.form.get("client_week_day")
    client_time = request.form.get("client_time")
    client_teacher = request.form.get("client_teacher")
    client_name = request.form.get("client_name")
    client_phone = request.form.get("client_phone")

    db.session.add(Bookings(
        client_week_day=client_week_day, 
        client_time=client_time, 
        client_teacher=client_teacher, 
        client_name=client_name, 
        client_phone=client_phone))
    db.session.commit()

    return render_template("booking_done.html", 
    client_week_day = client_week_day,
    client_time = client_time,
    client_name = client_name,
    client_phone = client_phone,
    week_days_ru_dict = week_days_ru_dict
    )

@app.route("/request/")
def render_request():

    class RequestForm(FlaskForm):
        request_client_name = StringField('request_client_name', validators=[DataRequired()])
        request_client_phone = StringField('request_client_phone', validators=[DataRequired()])
        request_goal = RadioField('request_goal', default='travel', validators=[DataRequired()], choices=[
            ('travel', 'Для путешествий'), 
            ('study', 'Для учебы'), 
            ('work', 'Для работы'), 
            ('relocate', 'Для переезда')
            ])
        request_time = RadioField('request_time', default='1-2', validators=[DataRequired()], choices=[
            ('1-2', '1-2 часа в неделю'), 
            ('3-5', '3-5 часов в неделю'), 
            ('5-7', '5-7 часов в неделю'), 
            ('7-10', '7-10 часов в неделю')
            ])    

    return render_template("request.html",
    form = RequestForm(),
    )

@app.route("/request_done/", methods=["POST"])
def render_request_done():
    goals = db.session.query(Goals).all()

    request_goal = request.form.get("request_goal")
    request_time = request.form.get("request_time")
    request_client_name = request.form.get("request_client_name")
    request_client_phone = request.form.get("request_client_phone")

    db.session.add(Requests(
        request_goal=request_goal, 
        request_time=request_time, 
        request_client_name=request_client_name, 
        request_client_phone=request_client_phone))
    db.session.commit()

    return render_template("request_done.html",
    goals = goals,
    request_goal = request_goal,
    request_time = request_time,
    request_client_name = request_client_name,
    request_client_phone = request_client_phone
    )

#app.run("0.0.0.0", 8000, debug=True)
if __name__ == "__main__":
    app.run()
