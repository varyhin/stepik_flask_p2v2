FROM debian:buster-slim

# Отключает интерактивный режим (меню) при установке пакетов
ENV DEBIAN_FRONTEND=noninteractive 
# Установка в «true» вызовет установку флага seen для вопросов заданных неинтерактивным интерфейсом.
ENV DEBCONF_NONINTERACTIVE_SEEN=true

ENV TZ=Europe/Moscow

RUN apt update && apt install -y \ 
  git \
  python3 \
  python3-pip

RUN pip3 install \
   gunicorn \
   Flask \
   flask_sqlalchemy \
   flask-wtf

# Можно вот так делать 
#COPY requirements.txt ./
#RUN pip3 install --no-cache-dir -r requirements.txt

COPY ./app /app 
WORKDIR app 


EXPOSE 8000
#CMD ["python3", "app.py"]
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "--log-level", "debug", "app:app"]
